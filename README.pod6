=begin pod

=NAME    Local::Zaaksysteem::DMARC
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Description

Nondescript

=head1 Installation

Install this module through L<zef|https://github.com/ugexe/zef>:

=begin code :lang<sh>
zef install Local::Zaaksysteem::DMARC
=end code

=head1 License

This module is distributed under the terms of the AGPL-3.0.

=end pod

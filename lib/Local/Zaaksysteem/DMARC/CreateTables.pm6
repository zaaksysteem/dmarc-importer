#! /usr/bin/env false

use v6.c;

use Config;
use Local::Zaaksysteem::DMARC::Repository::DmarcRecord;

unit module Local::Zaaksysteem::DMARC::CreateTables;

#| Create the tables required to store the DMARC records.
sub MAIN (
) is export {
	my $config-file = %*ENV<ZS_DMARC_CONFIG> // "dmarc.toml";

	if (!$config-file.IO.f) {
		note q:to/ERR/;
			Set a configuration file in the \$ZS_DMARC_CONFIG environment
			variable or create a "dmarc.toml" in the current working directory.
			ERR

		exit 2;
	}

	my $config = Config.new.read($config-file);

	if (!$config<database><path>) {
		note "database.path is not set in configuration.";
		exit 2;
	}

	if ($config<database><path>.IO.f) {
		note "Database is already created at $config<database><path>. Aborting.";
		exit 2;
	}

	Local::Zaaksysteem::DMARC::Repository::DmarcRecord.new(:$config).create-table;
}

=begin pod

=NAME    Local::Zaaksysteem::DMARC::CreateTables
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

perl6 -Ilib bin/create-tables

=head1 Description

Create the required tables for the other parts of this program to function.
Database credentials must be set in C<dmarc.toml>.

=head1 Examples

=begin input
perl6 -Ilib bin/create-tables
=end input

Creates the tables required. If anything goes wrong during the process, an error
will be thrown.

=end pod

# vim: ft=perl6 noet

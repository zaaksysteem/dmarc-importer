#! /usr/bin/env false

use v6.c;

use Config;
use File::Temp;
use File::XML::DMARC::Google;
use File::Zip;
use Local::Zaaksysteem::DMARC::Repository::DmarcRecord;

unit module Local::Zaaksysteem::DMARC::Import;

#| Import DMARC records into an SQLite database.
sub MAIN (
	#| The path to a directory containing DMARC email attachments. This can
	#| also point to a single DMARC report to import.
	*@import-paths,

	#| The path to a directory to store DMARC attachments in that have been
	#| processed already.
	Str:D :$archive-path = "/tmp",
) is export {
	my @records;

	@records.append(import($_.IO)) for @import-paths;

	my $config-file = %*ENV<ZS_DMARC_CONFIG> // "dmarc.toml";
	my $config = Config.new.read($config-file);
	my $repository = Local::Zaaksysteem::DMARC::Repository::DmarcRecord.new(:$config);

	$repository.create(@records);

	say "Imported @records.elems() new records.";
}

#| Import a zip file, which should contain a DMARC report in XML format.
multi sub import (
	IO::Path:D $target where { .f && .extension eq "zip" },
) {
	CATCH { .Str.note; return }

	my File::Zip $zip .= new($target);
	my $tempdir = tempdir;

	$zip.extract($tempdir);

	my @records;

	@records.append(samewith($_)) for $tempdir.IO.dir;

	@records;
}

#| Import a DMARC report in XML format.
multi sub import (
	IO::Path:D $target where { .f && .extension eq "xml" },
) {
	my %report = File::XML::DMARC::Google.new($target).contents;
	my @records;

	for %report<records>.list {
		@records.push(%(
			ip => $_<row><source-ip>,
			from => $_<identifiers><from>,
			count => $_<row><count>,
			dkim => $_<row><evaluated><dkim> // "",
			spf => $_<row><evaluated><spf> // "",
			datetime => %report<metadata><date-range><end>.posix,
		));
	}

	@records;
}

#| Import a while directory of files.
multi sub import (
	IO::Path:D $target where *.d
) {
	samewith($_) for $target.dir;
}

#| Fallback to show an error.
multi sub import (
	IO::Path:D $target
) {
	note "Not a valid path: $target.absolute()";
}

=begin pod

=NAME    Local::Zaaksysteem::DMARC::Import
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

perl6 -Ilib bin/import [--archive-path=C<archive-path>] C<path>

=head1 Description

Import the Google DMARC report zip files in the given I<path>. The records will
be parsed and saved in the database configured in C<dmarc.toml>. Optionally,
the C<--archive-path> can be given to specify a location where processed files
will be stored.

You can create the initial database using the C<bin/create-tables> script in
this repository.

=head1 Examples

=begin input
perl6 -Ilib bin/import --archive-path=/var/dmarc/done /var/dmarc/new
=end input

This will import all DMARC reports found in C</var/dmarc/new>, and store the
files in C</var/dmarc/done> once they've been imported into the database.

=end pod

# vim: ft=perl6 noet

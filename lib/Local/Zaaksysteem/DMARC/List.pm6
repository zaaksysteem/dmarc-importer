#! /usr/bin/env false

use v6.c;

use Config;
use Local::Zaaksysteem::DMARC::Repository::DmarcRecord;

unit module Local::Zaaksysteem::DMARC::List;

#| List existing records from the database.
sub MAIN (
	#| Limit the number of records used to generate the information.
	Int:D :$limit = 30,
) is export {
	my $config-file = %*ENV<ZS_DMARC_CONFIG> // "dmarc.toml";
	my $config = Config.new.read($config-file);
	my $repository = Local::Zaaksysteem::DMARC::Repository::DmarcRecord.new(:$config);
	my @records = $repository.get-recent($limit);
	my %stats;

	@records.map({
		%stats{$_<from>} += $_<count>;
	});

	for %stats.sort(*.value).reverse {
		say "{$_.key}: {$_.value}";
	}
}

=begin pod

=NAME    Local::Zaaksysteem::DMARC::List
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

perl6 -Ilib bin/list-records [--limit=I<limit=30>]

=head1 Description

Retrieve statistics using the last I<limit> records of data. The statistics
will be printed to C<STDOUT>, in the form of I<domain: count>. This is the
total number of mails on record for said domains, and will be sorted with the
highest number first.

This command is intended mostly to show the program is correctly importing data
from DMARC reports.

=head1 Examples

=begin input
perl6 -Ilib bin/list-records
=end input

=begin output
zaaksysteem.nl: 87
bwbrabant.api.zaaksysteem.nl: 35
voerendaal.api.zaaksysteem.nl: 16
sprint.zaaksysteem.nl: 10
hotfix.api.zaaksysteem.nl: 9
mintlab.api.zaaksysteem.nl: 4
bwbrabant-accept.api.zaaksysteem.nl: 1
=end output

=end pod

# vim: ft=perl6 noet

#! /usr/bin/env false

use v6.c;

use Config;
use DBIish;

unit class Local::Zaaksysteem::DMARC::Repository::DmarcRecord;

has Config $.config;

#| Insert a new DmarcRecord into the database.
multi method create (
	Associative:D %record
) {
	callsame([$_]);
}

#| Insert a list of new records into the database.
multi method create (
	@records
) {
	my $dbh = DBIish.connect("SQLite", :database($!config<database><path>));
	my $sth = $dbh.prepare(q:to/STMT/);
		INSERT INTO dmarc_records (`ip`, `from`, `count`, `dkim`, `spf`, `datetime`)
		VALUES (?, ?, ?, ?, ?, ?);
		STMT

	for @records {
		$sth.execute(
			$_<ip>,
			$_<from>,
			$_<count>,
			$_<dkim>,
			$_<spf>,
			$_<datetime>,
		);
	}

	$sth.finish;
	$dbh.dispose;
}

#| Create the table required for this repository.
method create-table (
) {
	my $dbh = DBIish.connect("SQLite", :database($!config<database><path>));

	$dbh.do(q:to/STMT/);
		CREATE TABLE dmarc_records (
			id INT,
			ip TEXT,
			`from` TEXT,
			`count` INT,
			dkim INT,
			spf INT,
			datetime INT
		);
		STMT
}

#| Get the most recent records.
method get-recent (
	Int:D $limit = 30,
) {
	my $dbh = DBIish.connect("SQLite", :database($!config<database><path>));
	my $sth = $dbh.prepare(q:to/STMT/);
		SELECT `from`, `count` FROM dmarc_records ORDER BY datetime DESC LIMIT 0, ?;
		STMT

	$sth.execute($limit);

	$sth.allrows.map({%(
		from => $_[0],
		count => $_[1],
	)});
}

=begin pod

=NAME    Local::Zaaksysteem::DMARC::Repository::DmarcRecord
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Description

This is the repository to interface with the database. It allows for storing
and retrieving records of DMARC reports. The repository accepts and returns
Perl 6 hashes to make is simple to work with and adjust in the future.

=head1 Examples

=begin code
use Local::Zaaksysteem::DMARC::Repository::DmarcRecord;

my @records = $repository.get-recent;
=end code

This produces a list of the 30 most recent added records.

=end pod

# vim: ft=perl6 noet
